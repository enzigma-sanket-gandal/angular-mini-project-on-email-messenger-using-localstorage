import { Injectable } from '@angular/core';
import { Message } from '../Model/message';

@Injectable({
  providedIn: 'root'
})
export class MessageServicesService {
  Messages: Message[]=[];
  Email: Message[]=[];

  constructor() {
      // this.messages = [];
   }
   addEmail(message:Message){
    this.Messages.push(message);
    let Messages = [];
    if(localStorage.getItem('Email') === null) {
      Messages = [];
      Messages.push(message);
      localStorage.setItem('Email', JSON.stringify(Messages));
    } else {
      Messages = JSON.parse(localStorage.getItem('Email') || '{}');
      Messages.push(message); 
      localStorage.setItem('Email', JSON.stringify(Messages));
    }
    }
    getEmailOnly()
    {
      if(localStorage.getItem('newMail') === null)
      {
        this.Email = [];
      }else {
        this.Email = JSON.parse(localStorage.getItem('newMail') || '{}');
      }
      return this.Email;
    }

    getEmail() {
      if(localStorage.getItem('Email') === null) {
        this.Messages = [];
      } else {
        this.Messages = JSON.parse(localStorage.getItem('Email') || '{}');
      }
      return this.Messages;
    }

    deleteMessage(message: Message){
      this.Messages = JSON.parse(localStorage.getItem('Email') || '{}');
      for (let i=0; i<this.Messages.length;i++){
        if(message.mainMessage === this.Messages[i].mainMessage)
        {
          this.Messages.splice(i,1);
          
          localStorage.setItem('Email',JSON.stringify(this.Messages));
        }
      }
    }
}
