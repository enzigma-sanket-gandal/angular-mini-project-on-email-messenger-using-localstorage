import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmptyMessageComponent } from './Components/empty-message/empty-message.component';
import { MessageBodyComponent } from './Components/message-body/message-body.component';
import { NewMessageComponent } from './Components/new-message/new-message.component';
import { PageNotFoundComponent } from './Components/page-not-found/page-not-found.component';

const routes: Routes = [
  {path:"",component:EmptyMessageComponent},
  {path:"msg-body",component:MessageBodyComponent},
  {path:"new-msg",component:NewMessageComponent},
  {path:'**',component:PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
