import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Message } from 'src/app/Model/message';
import { MessageServicesService } from 'src/app/services/message-services.service';

@Component({
  selector: 'app-message-list',
  templateUrl: './message-list.component.html',
  styleUrls: ['./message-list.component.css']
})
export class MessageListComponent implements OnInit {

  Messages: Message[]=[];
  constructor(public messageService: MessageServicesService,private router:Router) { 
    
  }

  ngOnInit(): void {
    this.Messages = this.messageService.getEmail();
  }
  async getEmailData(message:any)
  { 
    localStorage.removeItem('newMail');
    localStorage.setItem("newMail",JSON.stringify(message));
      await this.router.navigate(['/msg-body']);
      await location.reload();
  }
  deleteMessage(message:Message){
    if(confirm('Are you sure you want to delete this Message?'))
    {
      this.messageService.deleteMessage(message);
      
    }
  }

}
