import { Component, Input, OnInit } from '@angular/core';
import { Message } from 'src/app/Model/message';
import { MessageServicesService } from 'src/app/services/message-services.service';

@Component({
  selector: 'app-message-body',
  templateUrl: './message-body.component.html',
  styleUrls: ['./message-body.component.css']
})
export class MessageBodyComponent implements OnInit {
  @Input() Email:Message =JSON.parse(localStorage.getItem('newMail') || '{}')
   Messages: Message[]=[];
  constructor(public messageService: MessageServicesService) { 
    
  }

  ngOnInit(): void {
    this.Messages = this.messageService.getEmailOnly();
    // location.reload();
    // console.log(this.Email)
    // console.log(this.Messages);
  }

}
