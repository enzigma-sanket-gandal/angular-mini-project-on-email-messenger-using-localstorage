import { Component, OnInit } from '@angular/core';
import { Message } from 'src/app/Model/message';
import { MessageServicesService } from 'src/app/services/message-services.service';
import { MessageBodyComponent } from '../message-body/message-body.component';

@Component({
  selector: 'app-new-message',
  templateUrl: './new-message.component.html',
  styleUrls: ['./new-message.component.css']
})
export class NewMessageComponent implements OnInit {
    
  constructor(public messageService: MessageServicesService) { }
  ngOnInit(): void {

  }
  addMessage(to_mail: HTMLInputElement, cc_mail: HTMLInputElement,subjectMail:HTMLInputElement,mainMessage: HTMLTextAreaElement)
  {
      let today = new Date().toISOString().slice(0, 10)
      console.log(today);
      this.messageService.addEmail({
        to_mail:to_mail.value,
        cc_mail:cc_mail.value,
        subject_Mail:subjectMail.value,
        mainMessage:mainMessage.value,
        createdOn:today
      });
      to_mail.value="";
      cc_mail.value="";
      subjectMail.value="";
      mainMessage.value="";
  }
  
}
