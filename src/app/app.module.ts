import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './Components/navigation/navigation.component';
import { MessageListComponent } from './Components/message-list/message-list.component';
import { EmptyMessageComponent } from './Components/empty-message/empty-message.component';
import { MessageBodyComponent } from './Components/message-body/message-body.component';
import { NewMessageComponent } from './Components/new-message/new-message.component';
import { PageNotFoundComponent } from './Components/page-not-found/page-not-found.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    MessageListComponent,
    EmptyMessageComponent,
    MessageBodyComponent,
    NewMessageComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
